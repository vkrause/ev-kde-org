---
layout: page
title: "Charter of the Technical Working Group of the KDE e.V."
---

<h2>1 Purpose</h2>

  <p>
  The purpose of the Technical Working Group (TWG) is to define and execute the
  official software releases of KDE. It should also support and guide the
  processes of taking technical decisions within the KDE project. It has to make
  sure that the open source development process is kept intact and that the KDE
  community is involved in all important decisions.
  </p>

<h2>2 Open Source Process</h2>

  <p>
  The TWG has to respect, support and take into account the open source
  development process. This in particular means that it should support
  peer-review, maintainership, the "those who do the work decide" practice and
  strengthen the responsibilities and initiative of the individual contributors.
  </p>

  <p>
  The TWG should also respect and coordinate with existing decision making
  groups and processes where they exist in sub groups or sub projects of KDE.
  </p>

  <p>
  The TWG should also make sure that specific experts, especially those who
  work on the topics under decision, are involved in the decision making
  process.
  </p>

<h2>3 Responsibilities</h2>

  <h3>3.1 Release Schedule</h3>

    <p>
    The TWG is responsible for setting release schedules for the official KDE
    releases. This includes release dates, deadlines for individual release
    steps and restrictions for code changes.
    </p>

    <p>
    The TWG should coordinate release dates with the marketing and press efforts
    of KDE.
    </p>

  <h3>3.2 Definition of Release</h3>
  
    <p>
    The TWG defines what code is included in the official KDE releases. It
    decides which modules are released and which features are included based on
    the feature plan and criteria for inclusion of features. The criteria
    should be documented and publically available. The maintainers of the code
    have to be involved in the decision making process.
    </p>
    
    <p>
    The TWG decides which translations are included in a release. Other groups
    concerned with translations have to be involved in the decision.
    </p>

  <h3>3.3 Release Management</h3>
  
    <p>
    The TWG assigns one or more release managers for coordination of the
    official KDE releases. The release managers should coordinate with
    developers and packagers to organize packages for the release. They should
    also take care of branching and tagging the source code repository and
    prepare the source code packages released to the distributors and the
    public.
    </p>

  <h3>3.4 Reviewing applications</h3>

    <p>
    The TWG oversees which application and libraries are included in the modules
    of the source code repository. It should support existing decision making
    processes and make sure that decisions are taken in a coordinated and
    consistent way in the best interest of the KDE project.
    </p>

    <p>
    If it is not possible to reach consensus about individual decisions the
    TWG can specifically decide about inclusion of new applications and
    exclusion of obsolete, redundant or unmaintained applications. The
    maintainers of the applications have to be involved in the decision making
    process.
    </p>

    <p>
    The TWG is directly responsible for the core modules (kdelibs, kdereview 
    and kdebase). For other modules it should leave the responsibility to 
    module maintainers or specific decision making groups, where they exist.
    The TWG takes care of inter-module issues, balances interests between
    modules and oversees the modules released together in the official KDE
    releases as a whole.
    </p>

  <h3>3.5 Software Dependencies</h3>

    <p>
    The TWG decides about external or inter-module software dependencies.
    The TWG has to ensure that concrete decisions about tools and deployed
    versions are made and adhered to. All decisions should be taken
    transparently, involve the KDE community, in particular specific experts,
    and respect the needs of the various KDE contributors.
    </p>

  <h3>3.6 Communicating and moderating</h3>

    <p>
    The TWG summarizes decision making processes and helps finding consensus
    and moderates important discussions which affect greater areas of KDE. It
    should act as a technical contact for KDE in general.
    </p>

<h2>4 Members</h2>

  <p>
  The TWG consists of five elected members. They are elected by the membership of the
  KDE e.V. Only members of the KDE e.V. can be voted for the TWG. In addition, any
  of the voted members can select additional people to become part of the TWG and act
  in the role of the TWG.
  </p>

  <h3>4.1 Elections</h3>

    <p>
    The members of the TWG are elected by the membership of the KDE e.V. by
    online vote. The general rules for online votes on persons apply.
    </p>

    <p>
    Members of the TWG are elected for a term of one year.
    </p>
  
    <p>
    The board of the KDE e.V. is responsible for the execution of the election.
    </p>

  <h3>4.2 Extraordinary Elections</h3>
  
    <p>
    If at least seven members of the KDE e.V. demand an extraordinary election
    of the members of the TWG, the board of the KDE e.V. dissolves the
    membership of the TWG and holds an election of new members even if the term
    of one year has not passed yet.
    </p>
  
<h2>5 Transparency</h2>

  <p>
  The decision making process and other activities of the TWG should be
  transparent to the membership of the KDE e.V. and to the KDE community at
  large. The TWG should regularly report its activities to the KDE community
  and publically document all decisions taken.
  </p>
  
  <p>
  The TWG should involve the KDE community in the decision making process at
  all times. Discussions should preferably be held on public mailing lists like
  kde-core-devel.
  </p>
  
  <p>
  All policies on which decisions are based should be documented and publically
  available.
  </p>
  
<h2>6 Coordination</h2>

  <p>
  The TWG has to coordinate with other KDE e.V. working groups and the board of
  the KDE e.V. on all matters which do not exclusively fall under the
  responsibility of the TWG.
  </p>

  <p>
  The TWG should also provide the means to coordinate between groups
  working on different modules or different applications where no sufficient
  coordination mechanisms exist.
  </p>

<hr/>

<em>
<p>This charter has been decided by the membership of the KDE e.V. through
an online vote on November 7th 2006.</p>

<p>Currently there is no TWG in place as after an evaluation period it turned
out that the TWG concept was too formal to work in practice with the
self-organizing culture of the KDE development community. A less formally
organized release-team has emerged which has taken over the most important
responsibities of the TWG.</p>

</p>
</em>
