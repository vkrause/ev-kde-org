---
title: "Mitgliederversammlung KDE e.V. 2021"
layout: page
---

Die Mitgliederversammlung findet am Montag, den 21. Juni 2021 um 13:00 MESZ (CEST)  online in der passwortgeschützten Open-Source-Videokonferenz [BigBlueButton](https://bigbluebutton.org/) statt. Das System verfügt über eine Chatfunktion. Tagesordnung und Präsentationen werden über das Open-Source-Tool [OpenSlides](https://openslides.com/de) verwaltet und bereitgestellt. In OpenSlides wird auch die Anwesenheit der Mitglieder registriert, die Vertreter-Stimmen verwaltet sowie die anstehenden Wahlen durchgeführt. Eine sichere, geheime, und überprüfbare Wahl ist durch dieses System gewährleistet. Beide Systeme werden auf KDE-Servern betrieben. Alle Mitglieder haben im Vorfeld der Versammlung Zugriff auf die beiden Systeme erhalten. Die Liste der Teilnehmer wird zur Dokumentation aus OpenSlides exportiert.

Zu Beginn der Veranstaltung sind 90 Mitglieder anwesend. Sieben nicht anwesende Mitglieder haben je einen Vertreter benannt. Mitglieder ohne Stimmrecht sind nicht anwesend. Als Gast nimmt Petra Gillert, die Assistentin des Vorstandes, an der Veranstaltung teil.


## Tagesordnung

1. Begrüßung
2. Wahl des Versammlungsleiters
3. Bericht des Vorstands
   1. Bericht über Aktivitäten
   2. Bericht des Schatzmeisters
   3. Bericht der Rechnungsprüfer
   4. Entlastung des Vorstandes
4. Bericht der Vertreter und Arbeitsgruppen des Vereins
   1. Bericht der Vertreter in der KDE Free Qt Foundation
   2. Bericht der Advisory Board Working Group
   3. Bericht der Community Working Group
   4. Bericht der Financial Working Group
   5. Bericht der KDE Free Qt Working Group
   6. Bericht der Fundraising Working Group
   7. Bericht der Onboarding Working Group
   8. Bericht der System Administration Working Group
5. Wahl der Rechnungsprüfer
6. Wahl der Vertreter in der KDE Free Qt Foundation
7. Von Mitgliedern beantragte Wahlen
   1. Einstellung eines Geschäftsführers
   2. Fiduciary License Agreement 2.0
8. Verschiedenes

## Protokoll

Alle Berichte wurden den Mitgliedern im Vorfeld per Email zugesandt und am 19.06.2021 auf der durch den Verein organisierten Konferenz [Akademy](https://akademy.kde.org/2021) öffentlich vorgestellt.

### Begrüßung und Wahl des Versammlungsleiters

Um 13:00 eröffnet der Vorsitzende des Vorstands, Aleix Pol i Gonzàlez, die Versammlung. Er bedankt sich bei den Organisatoren der Mitgliederversammlung.

Frederik Gladhorn wird per Akklamation zum Versammlungsleiter gewählt.

Der Versammlungsleiter stellt fest, dass die Einladung zur Mitgliederversammlung ordnungsgemäß und fristgerecht - per Emailversand am 09.05.2021 mit vorläufiger und erneut am 07.06.2021 mit finaler Tagesordung - erfolgt ist. Es folgt eine kurze Fragerunde, weil wenige Mitglieder die Einladung angeblich nicht erhalten haben. Dies konnte geklärt werden. Es gibt keine Einwände.

Die Tagesordnung wird per Akklamation bestätigt.

Der Versammlungsleiter benennt Thomas Baumgart als Protokollanten und stellt fest, dass das satzungsgemäße Quorum erfüllt ist.

Mit dem Tool Openslides gibt es zu Beginn der Veranstaltung einige technische Probleme, so dass sich verschiedene Mitglieder nicht in das Wahlsystem einloggen können. Dies wird von der Systemadministration bis zur ersten Wahl im Hintergrund behoben.

### Bericht des Vorstands

#### Bericht über Aktivitäten

Der aktuelle Vorstand besteht aus Aleix Pol i Gonzàlez (Vorsitzender), Eike Hein (Schatzmeister und Stellvertreter des Vorsitzenden), Lydia Pintscher (Stellvertreterin des Vorsitzenden), Adriaan de Groot und Neofytos Kolokotronis.

23 neue aktive Mitglieder wurden in den Verein seit der letzten Mitgliederversammlung aufgenommen.

Der Verein hat erneut einige Fördermitglieder verloren. Im Vergleich zu 84 im letzten Jahr sind es in diesem Jahr nur noch 71. Der Vorstand hat damit begonnen, die Fördermitglieder in vierteljährlichem Abstand über die Arbeit mit einem Newsletter zu informieren und somit einer weiteren Fluktuation vorzubeugen. Die Probleme mit dem Verwaltungssystem CiviCRM und PayPal-Zahlungen bestehen weiterhin. Zwei neue Firmen-Fördermitglieder sind beigetreten. Dadurch erfolgte eine Erweiterung des aus den KDE-Patronen und ausgewählten Community-Partnern bestehende Advisory Board.

Der Verein hat eine Angestellte und sechs freie Mitarbeiter. Petra Gillert ist als Assistenz des Vorstands angestellt, Aniqa Khokhar und Paul Brown arbeiten als Marketing-Berater, Adam Szopa als Projekt-Koordinator und Allyson Alexandrou als Event-Koordinator. Neu hinzugekommen sind kürzlich Frederik Schwarzer und Carl Schwan für Dokumentationsarbeiten. Es gibt weitere Stellenausschreibungen für das Projekt 'Blauer Engel'. Die Aufgabengebiete der zuvor genannten werden dargestellt.

Soweit möglich ist für 2022 die Akademy wieder als Vor-Ort-Konferenz geplant.

Aleix Pol berichtet detailliert über den Sachstand einzelner im vergangenen Jahr vom Vorstand bearbeiteter Aktivitäten und gibt einen Ausblick über die des nächsten Jahres. KDE feiert am 14. Oktober 2021 seinen 25. Geburtstag. Der Vorstand bittet um Ideen aus der Mitgliedschaft, wie das Jubiläum genutzt und gefeiert werden kann.

#### Bericht des Schatzmeisters

Eike Hein erläutert den Finanzbericht, den er mit Hilfe der Financial Working Group und des Vorstands erstellt hat.

##### Finanzjahr 2020

Auf Grund der durch die Corona Pandemie bestehenden Ungewissheit über die Zahlungsbereitschaft der Fördermitglieder und Spender wurde eine konservative Ausgabenpolitik verfolgt um finanzielle Risiken zu vermeiden. Die Befürchtungen stellten sich jedoch nicht ein. Eine weitere Großspende der Handshake Foundation ermöglichte trotz des Rückgangs der Einnahmen aus Förderprogrammen wie dem Google Summer of Code eine stabile Finanzsituation zu bewahren, bei der die Einnahmen erneut über den Ausgaben lagen.

Eike stellt danach einge Details der Einnahmen und Ausgaben an Hand von grafischen Auswertungen dar.

##### Finanzplan 2021

Für 2021 rechnet Eike mit einer stabilen Einnahmensituation. Größere Events sind für 2021 nicht geplant. Von daher wird nicht mit hohen Reisekosten gerechnet. Er erläutert die geplanten Ausgaben für zusätzlich zu engagierende Freiberufler, die in diversen Bereichen und Projekten eingesetzt werden sollen. Diese Arbeiten sollen für mehr Visibilität des Vereins in der Gesellschaft sorgen. Der Vorstand erhofft sich dadurch höhere Einnahmen aus Zuschüssen und Spenden für die Zukunft. Die Mittel für diese Engagements stehen zur Verfügung.

Die Preise für Fördermitgliedschaften und Sponsoren stellen einen sensiblen Bereich dar, der nur sehr vorsichtig angepasst werden kann. Das Thema Fundraising gewinnt daher zukünftig eine größere Bedeutung um die Aktivitäten des Vereins in gewohnter Weise fortzuführen.

Anschließend werden Fragen aus der Mitgliedschaft zu den Berichten beantwortet.

#### Bericht der Kassenprüfer

Die Kassenprüfer haben in diesem Jahr die Prüfung bei einem Online-Treffen mit Lydia Pintscher und Petra Gellert durchgeführt und Einsicht in die Bücher genommen. Es wird eine ordnungsgemäße Buchführung attestiert. Alle Fragen der Kassenprüfer wurden beantwortet.

Die Kassenprüfer empfehlen die Entlastung des Vorstands für das Finanzjahr 2020.

#### Entlastung des Vorstands

Der Versammlungsleiter erklärt die Bedeutung der Entlastung des Vorstands und bittet um Meldung, ob Einwände gegen die Entlastung bestehen. Es gibt keine Meldungen.

Die Abstimmung darüber wird über die jetzt voll funktionsfähige Wahlfunktion des Online-Tools Openslides durchgeführt. Die Mitglieder des Vorstandes nehmen an dieser Abstimmung nicht teil. Das Abstimmungsergebnis lautet:

* Für die Entlastung:     **83**
* Gegen die Entlastung:    **1**
* Enthaltungen:            **3**
* Abstimmungsteilnehmer:  **87**

Mit diesem Ergebnis ist der Vorstand für die vergangene Berichtsperiode entlastet.

### Bericht der Vertreter und Arbeitsgruppen des Vereins

Hier werden von den einzelnen Gruppen lediglich Ergänzungen gegeben, die nicht für die Öffentlichkeit bestimmt sind und daher nicht in den vorab versandten Berichten und der öffentlichen Präsentation enthalten sind.

#### Bericht der Vertreter in der KDE Free Qt Foundation

Olaf Schmidt-Wischhöfer berichtet als Vertreter des KDE e.V. aus der KDE Free Qt Foundation. Die formelle Registrierung der neuen Präsidentschaft der Foundation ist noch nicht erfolgt. Dies ist in der verlangsamten Bearbeitung der norwegischen Behörden wegen der Corona Pandemie begründet.

Die beiden bisherigen Vertreter Olaf Schmidt-Wischhöfer und Martin Konold sind beide beruflich sehr eingespannt. Sie würden es daher begrüßen, wenn sich ein neuer Vertreter zur Wahl stellen würde. Frederik Gladhorn dankt den beiden für ihre langjährige und hervorragende Arbeit im Sinne des Vereins.

#### Andere Gruppen

Die anderen Gruppen haben über die in den öffentlich zugänglichen Informationen keine weiteren Punkte zu berichten.

### Wahl der Rechnungsprüfer

Die beiden bisherigen Rechnungsprüfer Andreas Cord-Landwehr und Ingo Klöcker stehen für eine Wiederwahl als Kassenprüfer zur Verfügung. Es gibt keine weiteren Vorschläge. Die abgegebenen Stimmen ergeben folgendes Wahlergebnis:

* Andreas Cord-Landwehr:   **91 Ja**
* Ingo Klöcker:            **91 Ja**
* Enhaltungen:              **6**
* Wahlteilnehmer:          **97**

Beide Kandidaten nehmen die Wahl nach Rückfrage durch den Versammlungsleiter an.

### Wahl der Vertreter in der KDE Free Qt Foundation

Olaf Schmidt-Wischhöfer steht für eine Wiederwahl zur Verfügung. Als weiterer Kandidat meldet sich Albert Astals Cid. Luis Falcon zieht seine Kandidatur vor dem Wahlgang zurück. Es gibt keine weiteren Vorschläge. Die Mitglieder haben die Wahl für einen oder beide Kandidaten zu stimmen oder sich der Stimme zu enthalten.

Damit sind die Kandidaten für die Wahl zum Verteter des KDE e.V. in der KDE Free Qt Foundation:

* Olaf Schmidt-Wischhöfer:    **90 Ja**
* Albert Astals Cid:          **85 Ja**
* Enhaltungen:                 **4**
* Wahlteilnehmer:             **96**

Beide Kandidaten nehmen die Wahl nach Rückfrage durch den Versammlungsleiter an.

### Von Mitgliedern beantragte Wahlen

Die beiden von den Mitgliedern eingebrachten Anträge auf Abstimmung sind fristgerecht beim Vorstand eingegangen.

#### Einstellung eines Geschäftsführers

Der von Cornelius Schumacher eingebrachte Antrag über die "Empfehlung der Mitgliedschaft an den Vorstand zur Einstellung eines Geschäftsführers" wurde den Mitgliedern vor der Versammlung per Email zur Verfügung gestellt und auch auf der Mailingliste der Mitglieder im Vorfeld diskutiert. Cornelius begründet seinen Antrag in der Versammlung. Aleix Pol stellt die derzeitige Meinung des Vorstandes über das Thema an Hand einer Präsentation dar. Er ermutigt die Mitglieder, selber aktiv zu werden, um den Vorstand zu entlasten und damit die Einstellung eines Geschäftsführers derzeit zu vermeiden. Nach kurzer Diskussion wird über den Antrag wie folgt abgestimmt:

* Annahme des Antrages:      **27**
* Ablehnung des Antrages:    **35**
* Enthaltungen:              **30**
* Wahlteilnehmer:            **92**

Der Antrag ist damit von der Mitgliedschaft abgelehnt.

#### Fiduciary License Agreement 2.0

Der Antrag wurde vom Antragsteller Adriaan de Groot zurückgezogen da die neue Fassung der Vereinbarung noch nicht zur Verfügung steht.

### Verschiedenes

Es werden keine Punkte diskutiert.

### Schluss der Versammlung

Frederik Gladhorn beendet die Versammlung um 15:46 und bittet nochmals um Entschuldigung für die am Anfang der Versammlung aufgetretenen technischen Probleme. Aleix Pol schließt die Mitgliederversammlung um 15:49 und bedankt sich bei Frederik für die Versammlungsleitung.


Die Versammlung fand frei von Ton- oder Videounterbrechungen statt.


Thomas Baumgart            Frederik Gladhorn
(Protokollant)             (Versammlungsleiter)
23.06.2021                 23.06.2021
