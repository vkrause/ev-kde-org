---
title: Careers in KDE
menu:
  main:
    parent: organization
    weight: 11
    name: Careers

# Set this to false if there are no open positions; otherwise, use a list
openpositions: false
---

{{< career >}}

Feel free to contact kde-ev-board<span>@</span>kde.org with any questions.

