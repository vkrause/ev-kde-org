---
title: "Forms"
menu:
  main:
    parent: documents
    weight: 4
---

+ [Application for Supporting Membership](supporting_member_application.pdf). 
  Fill out this form
  when applying for a corporate supporting membership.
+ [New Member Questionnaire](ev-questionnaire.text). 
  Ordinary
  members should fill in this questionaire when applying for membership in the
  KDE e.V. Please note that your membership has to be proposed by an existing
  member to the membership.
+ *Expense Report Forms*. Please fill out this 
  form when handing in receipts for costs the board has accepted to reimburse 
  you for. These forms do **not** apply for trips to sprints, conferences, etc. those
  are governed by the
  [Travel Cost Reimbursement Policy](/rules/reimbursement_policy/).
  * For reimbursement 
    [via PayPal](expense-paypal.pdf), use the 
    [PayPal form](expense-paypal.pdf).
  * For reimbursement 
    [via international bank transfer](expense-iban.pdf), use the 
    [international bank transfer (IBAN) form](expense-iban.pdf).
  * For reimbursement via other means or if you do not understand which of the above forms to use, use the 
    [traditional expense report form](expense_report.pdf).
+ [Proxy Instructions](proxy_instructions.pdf). If you are an
  active member of the KDE e.V. and are unable to attend the general assembly
  you can give a proxy (any other member who agrees to it, and does attend the general assembly)
  the right to vote for you. Fill out this form and make
  sure the board of KDE e.V. has received it (e.g. a scan sent by email) before the beginning
  of the assembly.
+ ["Vereinfachter Zuwendungsnachweis"](vereinfachter_zuwendungsnachweis_update2019.pdf).
  If you are donating money to the KDE e.V., you can
  use this form to make use of the German tax-exemption rules. For more 
  information, please refer to [the German tax information page](/donations-taxes-de/).
  (in German).
+ Fiduciary Licensing Documents. The FLA can *optionally* be used to assign the
  copyright in your contributions to KDE to KDE e.V.
  [General information](/rules/fla/) about the FLA describes why this
  document is important and why you might want to sign it.
    - **Older version** [FLA 1.3.5](FLA-1.3.5.pdf) (Fiduciary License Agreement) and
      [FRP 1.3.5](FRP-1.3.5.pdf) (Fiduciary Relicensing Policy) which is referred to by the FLA version 1.3.5.
+ [Agreement about local KDE organization](local_kde_org_agreement.pdf). 
  This agreement is used between KDE e.V. and a local
  organization representing KDE in a specific region. It grants the local
  organization the limited right to represent KDE locally and use the KDE
  trademark for that purpose. It's required that the local organization follows
  KDE e.V.'s goals and non-profit rules.
