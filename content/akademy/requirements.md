---
title: "Requirements for Akademy Location"
layout: page
---

Please see the [detailed requirements for 2022 (pdf)](/akademy/CallforHosts_2022.pdf) for more information about what is needed to host Akademy.
The most recent public call for hosts brochure is for 2022.
The current call for hosts is for **2024**.

## Call for Hosts

The KDE e.V. invites proposals to host Akademy, the yearly KDE contributors
conference and community summit, each year between June and September.
Akademy is the
biggest gathering of the community and includes a two-day conference, the
general assembly of the members of the KDE e.V., and a week of coding,
meeting, and discussing.

Proposals for Akademy are warmly welcomed, and should
aim for 180-300 attendees.

Proposals for Akademy should be sent to
<a href="mailto:kde-ev-board@kde.org">kde-ev-board@kde.org</a>.

Key points which proposals should consider, and which will be taken into
account when deciding among candidates, are:

- Local community support for hosting the conference
- The availability and cost of travel from major European cities
- The availability of sufficient low-cost accommodation
- The budget for infrastructure and facilities required to hold the conference
- The availability of restaurants or the organization of catering on-site
- Local industry and government support

See the [detailed requirements 2022 (pdf)](/akademy/CallforHosts_2022.pdf) for more information about what it needs to host Akademy.

The conference will require availability of facilities for one week,
including a weekend, during Summer (Northern hemisphere, June to September). Dates should avoid other key Free Software
conferences.

A few words of advice: organizing a conference of this size is hard work, but
there are many people in the community with experience who will be there to
help you. Bear in mind that people coming to these conferences do so
primarily to meet up with old friends and have fun, and so the hallway track
and social activities are very important.

> Deadline for **2024**:
Deadline for proposals is October 1st 2023, so that
the board has enough time to decide on proposals.

