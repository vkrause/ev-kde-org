From the 11th to the 12th of September, Kenny Coyle, Carl Schwan, and Lays Rodrigues met up for a mini-online sprint. The aim of the sprint was to discuss the implementation of the platforms needed for KDE's 25th Anniversary Fundraising Campaign.

The main goals of this sprint were to:

* Focus on the KDE 25th Anniversary
* Discuss how KDE can use this event to generate fundraising engagement
* Design messaging around the event
* Implement the campaign website
* Implement the fundraising platform
