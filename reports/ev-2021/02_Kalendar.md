<figure class="image-right">
    <img width="100%" src="images/Products/Kalendar_2.png" alt="Week view in Kalendar " />
    <figcaption>Kalendar is an advanced calendaring and task-managing app for both desktop and mobile devices.</figcaption>
</figure>

[Kalendar](https://apps.kde.org/en-gb/kalendar/) is a calendar application that allows you to manage your tasks and events. Kalendar supports both local calendars as well as a multitude of online calendars, including Nextcloud, Google® Calendar, Outlook®, Caldav, and many more.

Kalendar gives you many ways to interact with your events. The month view, as the name suggests, provides an overview of the entire month; the week view presents a detailed hour-by-hour overview of your week; and the schedule view lists all of your upcoming events so that you can easily and quickly plan ahead.

There is also a tasks view that makes it easy for you to manage your tasks and subtasks with Kalendar's powerful tree view and its customisable filtering capabilities.

Kalendar was built with the idea to be usable on your desktop machine, on mobile and everything in between.
