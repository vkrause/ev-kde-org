<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Products/duo-ok.png" alt="View of the two new KDE slimbook ultabooks." />
    </figure>
</div>

Spanish manufacturer and [KDE patron](https://ev.kde.org/supporting-members/) [Slimbook](https://slimbook.es/en/) released their new [KDE-themed Slimbook ultrabook](https://kde.slimbook.es/) in July of 2022.

The **KDE Slimbook 4** comes with a Ryzen 5700U processor and KDE's full-featured [Plasma desktop](https://kde.org/plasma-desktop/) running on [KDE Neon](https://neon.kde.org/). It also comes with dozens of Open Source programs and utilities pre-installed, and access to hundreds more through the Discover software manager app.

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Products/cover.jpg" alt="Cover of a Slimbook computer diaplying the KDE logo." />
    </figure>
</div>

The KDE Slimbook 4's AMD Ryzen 5700U processor is one of the most efficient CPUs for portable computers in the range. With its 8 GPU cores and 16 threads, it can run your whole office from home and on the go, render 3D animations, compile your code and serve up the entertainment for you during down time.

The Slimbook starts Plasma by default on Wayland, the state-of-the-art display server. With Wayland, you can enjoy the advantages of crisp fonts and images, frame rates adapted to each of your displays, and all the touchpad gestures implemented in Plasma.

Speaking of displays, the USB-C port lets you extend the desktop onto an external monitor, in addition to charging the laptop. Another nifty detail is the backlit black keyboard with keys engraved using the Noto Sans font--the same one used on the Plasma desktop.
